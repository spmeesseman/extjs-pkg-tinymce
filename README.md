# ExtJs Package - TinyMCE

[![Version](https://vsmarketplacebadge.apphb.com/version-short/spmeesseman.extjs-server-net.svg)](https://marketplace.visualstudio.com/items?itemName=spmeesseman.extjs-server-net)
[![Installs](https://vsmarketplacebadge.apphb.com/installs-short/spmeesseman.extjs-server-net.svg)](https://marketplace.visualstudio.com/items?itemName=spmeesseman.extjs-server-net)
[![Ratings](https://vsmarketplacebadge.apphb.com/rating-short/spmeesseman.extjs-server-net.svg)](https://marketplace.visualstudio.com/items?itemName=spmeesseman.extjs-server-net)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![Greenkeeper badge](https://badges.greenkeeper.io/spmeesseman/extjs-server-net.svg)](https://greenkeeper.io/)
[![Build Status](https://dev.azure.com/spmeesseman/extjs-server-net/_apis/build/status/spmeesseman.extjs-server-net?branchName=master)](https://dev.azure.com/spmeesseman/extjs-server-net/_build/latest?definitionId=2&branchName=master)

[![Known Vulnerabilities](https://snyk.io/test/github/spmeesseman/extjs-server-net/badge.svg)](https://snyk.io/test/github/spmeesseman/extjs-server-net)
[![codecov](https://codecov.io/gh/spmeesseman/extjs-server-net/branch/master/graph/badge.svg)](https://codecov.io/gh/spmeesseman/extjs-server-net)
[![Average time to resolve an issue](https://isitmaintained.com/badge/resolution/spmeesseman/extjs-server-net.svg)](https://isitmaintained.com/project/spmeesseman/extjs-server-net "Average time to resolve an issue")
[![Percentage of issues still open](https://isitmaintained.com/badge/open/spmeesseman/extjs-server-net.svg)](https://isitmaintained.com/project/spmeesseman/extjs-server-net "Percentage of issues still open")
[![Dependencies Status](https://david-dm.org/spmeesseman/extjs-server-net/status.svg)](https://david-dm.org/spmeesseman/extjs-server-net)
[![DevDependencies Status](https://david-dm.org/spmeesseman/extjs-server-net/dev-status.svg)](https://david-dm.org/spmeesseman/extjs-server-net?type=dev)

## Description

> This package provides a fully integrated stack between and ExtJs client and ASP.NET WebAPI.

## Usage

The main javascript:

    mce-release\version

To include the package in the build, be sure to add the package name to the list of required
packages in the application's app.json file:

    "requires": [
         "tinymce",
        ...
    ]

